/*
 * format_sh: Format backslash-newline combos in a shell script.
 * Copyright (C) 2018  Walid M. Boudelal
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <errno.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <fcntl.h>
#include <unistd.h>

#define TABSTOP_SIZE 010
#define MAX_LINE_LENGTH 511
#define DEFAULT_TEXT_WDITH 78

static const char *progname;
static const char sentinel[1] = {0};
static const size_t tabstop = TABSTOP_SIZE;

FILE *open_file(const char *pathname, const char *mode);
void close_files_(void *file, ...);
int process_file(FILE *in, FILE *out, size_t text_width);
size_t get_screen_len(const char *str);

#define close_files(...) close_files_(__VA_ARGS__, (void *) sentinel)

int main(int argc, char *argv[])
{
	FILE *in = NULL;
	FILE *out = NULL;
	size_t text_width = DEFAULT_TEXT_WDITH;
	int r;

	progname = *argv;

	switch (argc) {
	case 1:
		in = stdin;
		out = stdout;
		break;
	case 2:
		in = open_file(argv[1], "r");
		if (!in)
			goto open_file_error;
		out = stdout;
		break;
	case 3:
		in = open_file(argv[1], "r");
		if (!in)
			goto open_file_error;
		out = open_file(argv[2], "w");
		break;
	case 4:
		in = open_file(argv[1], "r");
		if (!in)
			goto open_file_error;
		out = open_file(argv[2], "w");
		if (!out)
			goto open_file_error;
		text_width = (size_t) strtoul(argv[3], NULL, 0);
		if (!text_width || text_width >= MAX_LINE_LENGTH - 1) {
			fprintf(stderr, "%s: invalid text width: %zu; "
				"using default (%zu)\n",
				progname, (size_t) text_width,
				(size_t) DEFAULT_TEXT_WDITH);
			text_width = DEFAULT_TEXT_WDITH;
		}
		break;
	default:
		fprintf(stderr, "%s: invalid number of arguments\n",
			progname);
		break;
	}

	if (!(in && out))
		goto open_file_error;

	r = process_file(in, out, text_width);

	if (r != 0)
		perror(NULL);

	close_files(in, out);

	exit(r == 0 ? EXIT_SUCCESS : EXIT_FAILURE);

open_file_error:
	close_files(in, out);
	exit(EXIT_FAILURE);
}

#define ACCEPTED_MODES "\"w\" and \"r\""
FILE *open_file(const char *pathname, const char *mode)
{
	FILE *f;
	bool file_exists;
	char buf[100];

	if (!pathname || !mode)
		goto null_arguments_error;

	if (!((mode[0] == 'w' || mode[0] == 'r') && mode[1] == '\0'))
		goto invalid_mode_error;

	if (strncmp(pathname, "-", 2) == 0) {
		/* This means either stdin or stdout */
		switch (mode[0]) {
		case 'r':
			return stdin;
			break;
		case 'w':
			return stdout;
			break;
		default:
			/* This should be impossible to reach */
			break;
		}
	}


	file_exists = faccessat(AT_FDCWD, pathname, F_OK, AT_EACCESS) == 0;

	if (mode[0] == 'w' && file_exists)
		/* Refuse to overwrite existing files. */
		goto file_exists_error;
	else if (mode[0] == 'r' && !file_exists)
		/* Attempt to read a file that doesn't exist. */
		goto file_doesnt_exist_error;

	f = fopen(pathname, mode);

	if (!f)
		goto failed_to_open_error;

	return f;

null_arguments_error:
	if (!pathname)
		fprintf(stderr, "%s: %s: pathname is NULL\n",
			progname, __func__);
	if (!mode)
		fprintf(stderr, "%s: %s: mode is NULL\n",
			progname, __func__);
	return NULL;

invalid_mode_error:
	fprintf(stderr, "%s: %s: mode (%s) is invalid. This function only "
		"accepts the modes %s\n",
		progname, __func__, mode, ACCEPTED_MODES);
	return NULL;

file_exists_error:
	fprintf(stderr, "%s: %s: refusing to overwrite existing file %s\n",
		progname, __func__, pathname);
	return NULL;

file_doesnt_exist_error:
	strerror_r(errno, buf, 100);
	fprintf(stderr, "%s: %s: %s: %s: "
		"attempt to read a non-existing file\n",
		progname, __func__, pathname, buf);
	errno = 0;
	return NULL;

failed_to_open_error:
	if (errno) {
		strerror_r(errno, buf, 100);
		fprintf(stderr, "%s: %s: %s: %s: failed to open file.\n",
			progname, __func__, pathname, buf);
	} else {
		fprintf(stderr, "%s: %s: failed to open file %s.\n",
			progname, __func__, pathname);
	}
	return NULL;
}
#undef ACCEPTED_MODES

void close_files_(void *file, ...)
{
	va_list ap;
	void *f;

	va_start(ap, file);

	f = file;
	while (f != sentinel) {
		if (f && f != stdin && f != stdout && f != stderr)
			fclose((FILE *) f);
		f = va_arg(ap, void *);
	}

	va_end(ap);

	return;
}

int process_file(FILE *in, FILE *out, size_t text_width)
{
	char buf[MAX_LINE_LENGTH + 1];

	if (text_width >= MAX_LINE_LENGTH - 1)
		text_width = MAX_LINE_LENGTH - 1;

	while (fgets(buf, MAX_LINE_LENGTH, in) != NULL) {
		size_t len = strlen(buf);
		char *backslashp;
		size_t iterations;
		size_t screen_len;

		if (len <= 3)
			goto next_iteration;

		backslashp = buf + len - 3;

		/* If the line is not of the format:
		 * "xxxxxxxxxxxxxx \\\n"
		 * then skip it */
		if (strncmp(backslashp, " \\\n", 3) != 0)
			goto next_iteration;

		screen_len = get_screen_len(buf);

		if (screen_len >= text_width)
			goto next_iteration;

		iterations = text_width - screen_len + 1;
		while (iterations--)
			*backslashp++ = ' ';

		*backslashp++ = '\\';
		*backslashp++ = '\n';
		*backslashp++ = '\0';

next_iteration:
		fputs(buf, out);
	}

	return feof(in) && !ferror(in) ? 0 : -1;
}

size_t get_screen_len(const char *str)
{
	size_t len;
	char c;

	if (!str)
		return 0;

	len = 0;
	while ((c = *str++) != '\0' && c != '\n')
		if (c == '\t')
			len += tabstop - (len % tabstop);
		else
			++len;

	return len;
}
