# format_sh

This program formats this:

```
         1         2         3         4         5         6         7
123456789012345678901234567890123456789012345678901234567890123456789012345678
xx xxx x xxxxxxxx x xx x \
        xxx x xxxxx \
        xxxxxxx x xxxxx x xxxxx xxx \
xxxx xxxxx xxxxx \
xxxx
```

into this:

```
         1         2         3         4         5         6         7
123456789012345678901234567890123456789012345678901234567890123456789012345678
xx xxx x xxxxxxxx x xx x                                                     \
        xxx x xxxxx                                                          \
        xxxxxxx x xxxxx x xxxxx xxx                                          \
xxxx xxxxx xxxxx                                                             \
xxxx
```

In this example, the text width is 78, but this program can do it for any
given text width.
